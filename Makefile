PROJECT=link-common
PREFIX=$(shell pwd)
VERSION=$(shell git describe --match 'v[0-9]*'  --always)
DEFAULT_BRANCH=$(shell git rev-parse --abbrev-ref HEAD)

ifndef BRANCH
	BRANCH=$(DEFAULT_BRANCH)
endif

ifdef CI_COMMIT_REF_SLUG
	BRANCH=$(CI_COMMIT_REF_SLUG)
endif

ifndef PROTOC
	PROTOC=protoc
endif

SOURCE_DIR=$(PREFIX)
PROTO_DIR=$(PREFIX)/proto
DEFS_DIR=$(PREFIX)/defs
DOCS_DIR=$(PREFIX)/docs

.PHONY: all proto protodoc
.DEFAULT: all

# Targets
all: summary proto protodoc

summary:
	@printf "\033[1;37m  == \033[1;32m$(PROJECT) \033[1;33m$(VERSION) \033[1;37m==\033[0m\n"
	@printf "    Platform : \033[1;37m$(shell uname -sr)\033[0m\n"
	@printf "    Protoc   : \033[1;37m`$(PROTOC) --version`\033[0m\n"
	@printf "    Git      : \033[1;37m$(shell git version)\033[0m\n"
	@printf "    Branch   : \033[1;37m$(BRANCH)\033[0m\n"
	@echo

proto:
	@mkdir -p $(PROTO_DIR)
	@printf "\033[1;35m  Compiling protos ...\033[0m\n"
	@$(PROTOC) --proto_path=$(PROTO_DIR) --micro_out=$(PROTO_DIR) --go_out=$(PROTO_DIR) $(PROTO_DIR)/*.proto
	@printf "\033[1;32m    Done\033[0m\n"
	@echo

protodoc:
	@mkdir -p $(DOCS_DIR)
	@printf "\033[1;35m  Build proto doc ...\033[0m\n"
	@$(PROTOC) --doc_out=docs --proto_path=$(PROTO_DIR) --doc_opt=markdown,$(DOCS_DIR)/proto.md $(PROTO_DIR)/*.proto
	@printf "\033[1;32m    Done\033[0m\n"
	@echo

upgrade:
	@printf "\033[1;36m  Upgrading dependences ...\033[0m\n"
	@go get -u ./...
	@go mod tidy
	@echo
