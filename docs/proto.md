# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [gateway.proto](#gateway-proto)
    - [GetGatewayRequest](#-GetGatewayRequest)
    - [GetGatewayResponse](#-GetGatewayResponse)
    - [ListGatewayRequest](#-ListGatewayRequest)
    - [ListGatewayResponse](#-ListGatewayResponse)
    - [SelectGatewayRequest](#-SelectGatewayRequest)
    - [SelectGatewayResponse](#-SelectGatewayResponse)
    - [gateway](#-gateway)
    - [select_summary](#-select_summary)
  
    - [LinkGateway](#-LinkGateway)
  
- [instruction.proto](#instruction-proto)
    - [RemoveAccountRequest](#-RemoveAccountRequest)
    - [RemoveAccountResponse](#-RemoveAccountResponse)
    - [RemoveDeviceRequest](#-RemoveDeviceRequest)
    - [RemoveDeviceResponse](#-RemoveDeviceResponse)
    - [RemoveSessionRequest](#-RemoveSessionRequest)
    - [RemoveSessionResponse](#-RemoveSessionResponse)
  
    - [LinkInstruction](#-LinkInstruction)
  
- [message.proto](#message-proto)
    - [SendAccountRequest](#-SendAccountRequest)
    - [SendAccountResponse](#-SendAccountResponse)
    - [SendDeviceRequest](#-SendDeviceRequest)
    - [SendDeviceResponse](#-SendDeviceResponse)
    - [SendGlobalRequest](#-SendGlobalRequest)
    - [SendGlobalResponse](#-SendGlobalResponse)
    - [SendGroupRequest](#-SendGroupRequest)
    - [SendGroupResponse](#-SendGroupResponse)
    - [SendSessionRequest](#-SendSessionRequest)
    - [SendSessionResponse](#-SendSessionResponse)
  
    - [LinkMessage](#-LinkMessage)
  
- [session.proto](#session-proto)
    - [GetSessionRequest](#-GetSessionRequest)
    - [GetSessionResponse](#-GetSessionResponse)
    - [ListSessionRequest](#-ListSessionRequest)
    - [ListSessionResponse](#-ListSessionResponse)
  
    - [LinkSession](#-LinkSession)
  
- [stat.proto](#stat-proto)
    - [CheckAccountRequest](#-CheckAccountRequest)
    - [CheckAccountResponse](#-CheckAccountResponse)
    - [CheckAccountResponse.OnlineEntry](#-CheckAccountResponse-OnlineEntry)
    - [CheckDeviceRequest](#-CheckDeviceRequest)
    - [CheckDeviceResponse](#-CheckDeviceResponse)
    - [CheckDeviceResponse.OnlineEntry](#-CheckDeviceResponse-OnlineEntry)
    - [CheckSessionRequest](#-CheckSessionRequest)
    - [CheckSessionResponse](#-CheckSessionResponse)
    - [CheckSessionResponse.OnlineEntry](#-CheckSessionResponse-OnlineEntry)
    - [OnlineAccountListResponse](#-OnlineAccountListResponse)
    - [OnlineAccountListResponse.OnlineEntry](#-OnlineAccountListResponse-OnlineEntry)
    - [OnlineCountRequest](#-OnlineCountRequest)
    - [OnlineCountResponse](#-OnlineCountResponse)
    - [OnlineDeviceListResponse](#-OnlineDeviceListResponse)
    - [OnlineDeviceListResponse.OnlineEntry](#-OnlineDeviceListResponse-OnlineEntry)
    - [OnlineListRequest](#-OnlineListRequest)
    - [OnlineSessionListResponse](#-OnlineSessionListResponse)
    - [RefreshStatRequest](#-RefreshStatRequest)
    - [RefreshStatResponse](#-RefreshStatResponse)
    - [online_detail](#-online_detail)
    - [online_detail_list](#-online_detail_list)
  
    - [LinkStat](#-LinkStat)
  
- [tools.proto](#tools-proto)
    - [SendUpwardRequest](#-SendUpwardRequest)
    - [SendUpwardResponse](#-SendUpwardResponse)
  
    - [LinkUpward](#-LinkUpward)
  
- [trace.proto](#trace-proto)
    - [StartTraceRequest](#-StartTraceRequest)
    - [StartTraceResponse](#-StartTraceResponse)
    - [StopTraceRequest](#-StopTraceRequest)
    - [StopTraceResponse](#-StopTraceResponse)
  
    - [StartTraceRequest.TracerType](#-StartTraceRequest-TracerType)
  
    - [LinkTrace](#-LinkTrace)
  
- [Scalar Value Types](#scalar-value-types)



<a name="gateway-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## gateway.proto



<a name="-GetGatewayRequest"></a>

### GetGatewayRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  | 网关ID |






<a name="-GetGatewayResponse"></a>

### GetGatewayResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| gateway | [gateway](#gateway) |  | 网关信息 |






<a name="-ListGatewayRequest"></a>

### ListGatewayRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| svc | [string](#string) |  | 网关类型（可留空） |






<a name="-ListGatewayResponse"></a>

### ListGatewayResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| gateways | [gateway](#gateway) | repeated | 网关列表 |






<a name="-SelectGatewayRequest"></a>

### SelectGatewayRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| account_id | [int64](#int64) |  | 账号ID |
| platform | [string](#string) |  | 平台 |
| version | [string](#string) |  | 客户端版本 |
| device | [string](#string) |  | 设备标识（指纹） |
| svc | [string](#string) |  | 网关类型（默认ws） |
| total | [int64](#int64) |  | 希望获取网关总数（默认5） |






<a name="-SelectGatewayResponse"></a>

### SelectGatewayResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| gateways | [select_summary](#select_summary) | repeated | 网关列表 |






<a name="-gateway"></a>

### gateway



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  | 网关ID |
| advertise_address | [string](#string) |  | 网关地址 |
| current_sessions | [int64](#int64) |  | 当前连接数 |
| current_groups | [int64](#int64) |  | 当前群组数（包括空群组） |
| current_accounts | [int64](#int64) |  | 当前账号数 |
| current_devices | [int64](#int64) |  | 当前设备数 |
| total_upward_msgs | [int64](#int64) |  | 总上行消息条数 |
| total_downward_msgs | [int64](#int64) |  | 总下行消息条数 |
| total_connects | [int64](#int64) |  | 总创建连接次数 |
| total_registers | [int64](#int64) |  | 总注册成功次数 |
| total_binds | [int64](#int64) |  | 总绑定群组次数 |
| mem_alloc | [uint64](#uint64) |  | 进程当前分配内存 |
| total_mem_alloc | [uint64](#uint64) |  | 进程总分配内存 |
| allocs | [uint64](#uint64) |  | 进程分配内存次数 |
| frees | [uint64](#uint64) |  | 进程释放内存次数 |
| num_cpus | [int32](#int32) |  | 当前逻辑CPU个数 |
| num_goroutines | [int32](#int32) |  | 当前Goroutine数 |
| cpu_idle | [uint64](#uint64) |  | CPU空闲时间片 |
| cpu_no_idle | [uint64](#uint64) |  | CPU工作时间片 |
| mem_total | [uint64](#uint64) |  | 总内存数 |
| mem_available | [uint64](#uint64) |  | 可用内存数 |
| svc | [string](#string) |  | 网关类型 |






<a name="-select_summary"></a>

### select_summary



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  | 网关ID |
| advertise_address | [string](#string) |  | 网关地址 |
| sign | [string](#string) |  | 签名 |
| svc | [string](#string) |  | 网关类型 |





 

 

 


<a name="-LinkGateway"></a>

### LinkGateway
Services of gateway

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| List | [.ListGatewayRequest](#ListGatewayRequest) | [.ListGatewayResponse](#ListGatewayResponse) | 网关列表 |
| Get | [.GetGatewayRequest](#GetGatewayRequest) | [.GetGatewayResponse](#GetGatewayResponse) | 获取网关信息 |
| Select | [.SelectGatewayRequest](#SelectGatewayRequest) | [.SelectGatewayResponse](#SelectGatewayResponse) | 分配网关 |

 



<a name="instruction-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## instruction.proto



<a name="-RemoveAccountRequest"></a>

### RemoveAccountRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| account_id | [int64](#int64) |  | 账号ID |
| group_id | [int64](#int64) |  | 群组ID（可留空） |






<a name="-RemoveAccountResponse"></a>

### RemoveAccountResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ok | [bool](#bool) |  | 是否成功（不代表结果） |






<a name="-RemoveDeviceRequest"></a>

### RemoveDeviceRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| device | [string](#string) |  | 设备标识（指纹） |






<a name="-RemoveDeviceResponse"></a>

### RemoveDeviceResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ok | [bool](#bool) |  | 是否成功 |






<a name="-RemoveSessionRequest"></a>

### RemoveSessionRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| session | [string](#string) |  | 连接Session |






<a name="-RemoveSessionResponse"></a>

### RemoveSessionResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ok | [bool](#bool) |  | 是否成功 |





 

 

 


<a name="-LinkInstruction"></a>

### LinkInstruction
Service of instruction

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| RemoveSession | [.RemoveSessionRequest](#RemoveSessionRequest) | [.RemoveSessionResponse](#RemoveSessionResponse) | 移除（踢）连接 |
| RemoveAccount | [.RemoveAccountRequest](#RemoveAccountRequest) | [.RemoveAccountResponse](#RemoveAccountResponse) | 移除（踢）账号 |
| RemoveDevice | [.RemoveDeviceRequest](#RemoveDeviceRequest) | [.RemoveDeviceResponse](#RemoveDeviceResponse) | 移除（踢）设备 |

 



<a name="message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## message.proto



<a name="-SendAccountRequest"></a>

### SendAccountRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| type | [int32](#int32) |  | 消息类型 |
| priority | [int32](#int32) |  | 消息优先级 |
| payload | [string](#string) |  | 消息内容 |
| to_account | [int64](#int64) |  | 接收账号 |
| to_group | [int64](#int64) |  | 接收群组（可留空）（如不为空，标识只发给该群组下的对应账号） |
| from | [int64](#int64) |  | 发送者 |






<a name="-SendAccountResponse"></a>

### SendAccountResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ok | [bool](#bool) |  | 是否发送成功 |
| message_id | [int64](#int64) |  | 消息ID |






<a name="-SendDeviceRequest"></a>

### SendDeviceRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| type | [int32](#int32) |  | 消息类型 |
| priority | [int32](#int32) |  | 消息优先级 |
| payload | [string](#string) |  | 消息内容 |
| to_device | [string](#string) |  | 接收设备 |
| from | [int64](#int64) |  | 发送者 |






<a name="-SendDeviceResponse"></a>

### SendDeviceResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ok | [bool](#bool) |  | 是否发送成功 |
| message_id | [int64](#int64) |  | 消息ID |






<a name="-SendGlobalRequest"></a>

### SendGlobalRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| type | [int32](#int32) |  | 消息类型 |
| priority | [int32](#int32) |  | 消息优先级 |
| payload | [string](#string) |  | 消息内容 |
| to_group | [int64](#int64) |  | 组别（大于0：所有在组中的连接、0：所有连接、小于0：所有不在组中的连接） |
| from | [int64](#int64) |  | 发送者（账号ID） |






<a name="-SendGlobalResponse"></a>

### SendGlobalResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ok | [bool](#bool) |  | 是否发送成功（仅标识接收成功状态，不代表完全投递） |
| message_id | [int64](#int64) |  | 消息ID |






<a name="-SendGroupRequest"></a>

### SendGroupRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| type | [int32](#int32) |  | 消息类型 |
| priority | [int32](#int32) |  | 消息优先级 |
| payload | [string](#string) |  | 消息内容 |
| to_group | [int64](#int64) |  | 接收群组 |
| from | [int64](#int64) |  | 发送者 |






<a name="-SendGroupResponse"></a>

### SendGroupResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ok | [bool](#bool) |  | 是否发送成功 |
| message_id | [int64](#int64) |  | 消息ID |






<a name="-SendSessionRequest"></a>

### SendSessionRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| type | [int32](#int32) |  |  |
| priority | [int32](#int32) |  |  |
| payload | [string](#string) |  |  |
| to_session | [string](#string) |  |  |
| from | [int64](#int64) |  |  |






<a name="-SendSessionResponse"></a>

### SendSessionResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ok | [bool](#bool) |  |  |
| message_id | [int64](#int64) |  |  |





 

 

 


<a name="-LinkMessage"></a>

### LinkMessage
Service of message

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| SendGlobal | [.SendGlobalRequest](#SendGlobalRequest) | [.SendGlobalResponse](#SendGlobalResponse) | 发送全局消息 |
| SendGroup | [.SendGroupRequest](#SendGroupRequest) | [.SendGroupResponse](#SendGroupResponse) | 发送群组消息 |
| SendAccount | [.SendAccountRequest](#SendAccountRequest) | [.SendAccountResponse](#SendAccountResponse) | 发送账号消息（对单人) |
| SendDevice | [.SendDeviceRequest](#SendDeviceRequest) | [.SendDeviceResponse](#SendDeviceResponse) | 发送设备消息 |
| SendSession | [.SendSessionRequest](#SendSessionRequest) | [.SendSessionResponse](#SendSessionResponse) | 发送连接消息 |

 



<a name="session-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## session.proto



<a name="-GetSessionRequest"></a>

### GetSessionRequest







<a name="-GetSessionResponse"></a>

### GetSessionResponse







<a name="-ListSessionRequest"></a>

### ListSessionRequest







<a name="-ListSessionResponse"></a>

### ListSessionResponse






 

 

 


<a name="-LinkSession"></a>

### LinkSession
Service of session

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| Get | [.GetSessionRequest](#GetSessionRequest) | [.GetSessionResponse](#GetSessionResponse) |  |
| List | [.ListSessionRequest](#ListSessionRequest) | [.ListSessionResponse](#ListSessionResponse) |  |

 



<a name="stat-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## stat.proto



<a name="-CheckAccountRequest"></a>

### CheckAccountRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| account_ids | [int64](#int64) | repeated | 账号ID（列表） |
| svc | [string](#string) |  | 网关类型（可留空） |






<a name="-CheckAccountResponse"></a>

### CheckAccountResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| online | [CheckAccountResponse.OnlineEntry](#CheckAccountResponse-OnlineEntry) | repeated | 账号ID =&gt; 是否在线 |






<a name="-CheckAccountResponse-OnlineEntry"></a>

### CheckAccountResponse.OnlineEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [int64](#int64) |  |  |
| value | [bool](#bool) |  |  |






<a name="-CheckDeviceRequest"></a>

### CheckDeviceRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| devices | [string](#string) | repeated | 设备标识（列表） |
| svc | [string](#string) |  | 网关类型（可留空） |






<a name="-CheckDeviceResponse"></a>

### CheckDeviceResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| online | [CheckDeviceResponse.OnlineEntry](#CheckDeviceResponse-OnlineEntry) | repeated | 设备标识 =&gt; 是否在线 |






<a name="-CheckDeviceResponse-OnlineEntry"></a>

### CheckDeviceResponse.OnlineEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [bool](#bool) |  |  |






<a name="-CheckSessionRequest"></a>

### CheckSessionRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| session_ids | [string](#string) | repeated | 连接ID（列表） |
| svc | [string](#string) |  | 网关类型（可留空） |






<a name="-CheckSessionResponse"></a>

### CheckSessionResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| online | [CheckSessionResponse.OnlineEntry](#CheckSessionResponse-OnlineEntry) | repeated | 连接ID =&gt; 是否在线 |






<a name="-CheckSessionResponse-OnlineEntry"></a>

### CheckSessionResponse.OnlineEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [bool](#bool) |  |  |






<a name="-OnlineAccountListResponse"></a>

### OnlineAccountListResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| online | [OnlineAccountListResponse.OnlineEntry](#OnlineAccountListResponse-OnlineEntry) | repeated | 在线列表 |






<a name="-OnlineAccountListResponse-OnlineEntry"></a>

### OnlineAccountListResponse.OnlineEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [int64](#int64) |  |  |
| value | [online_detail_list](#online_detail_list) |  |  |






<a name="-OnlineCountRequest"></a>

### OnlineCountRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| group_id | [int64](#int64) |  | 群组ID |
| gateway | [string](#string) |  | 网关ID |
| svc | [string](#string) |  | 网关类型（可留空） |






<a name="-OnlineCountResponse"></a>

### OnlineCountResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| count | [int64](#int64) |  | 总数 |






<a name="-OnlineDeviceListResponse"></a>

### OnlineDeviceListResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| online | [OnlineDeviceListResponse.OnlineEntry](#OnlineDeviceListResponse-OnlineEntry) | repeated | 在线列表 |






<a name="-OnlineDeviceListResponse-OnlineEntry"></a>

### OnlineDeviceListResponse.OnlineEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [online_detail_list](#online_detail_list) |  |  |






<a name="-OnlineListRequest"></a>

### OnlineListRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| group_id | [int64](#int64) |  | 群组ID |
| gateway | [string](#string) |  | 网关ID |
| svc | [string](#string) |  | 网关类型（可留空） |






<a name="-OnlineSessionListResponse"></a>

### OnlineSessionListResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| online | [online_detail_list](#online_detail_list) |  | 在线列表 |






<a name="-RefreshStatRequest"></a>

### RefreshStatRequest







<a name="-RefreshStatResponse"></a>

### RefreshStatResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ok | [bool](#bool) |  | 是否刷新成功 |






<a name="-online_detail"></a>

### online_detail



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| session_id | [string](#string) |  | 连接ID |
| gateway_id | [string](#string) |  | 网关ID |
| remote_addr | [string](#string) |  | 远端地址 |
| device | [string](#string) |  | 设备标识（指纹） |
| account_id | [int64](#int64) |  | 账号ID |
| group_id | [int64](#int64) |  | 群组ID |






<a name="-online_detail_list"></a>

### online_detail_list



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| list | [online_detail](#online_detail) | repeated | 列表 |





 

 

 


<a name="-LinkStat"></a>

### LinkStat
Service of stat

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| OnlineSessionCount | [.OnlineCountRequest](#OnlineCountRequest) | [.OnlineCountResponse](#OnlineCountResponse) | 获取在线连接数 |
| OnlineAccountCount | [.OnlineCountRequest](#OnlineCountRequest) | [.OnlineCountResponse](#OnlineCountResponse) | 获取在线账号数 |
| OnlineDeviceCount | [.OnlineCountRequest](#OnlineCountRequest) | [.OnlineCountResponse](#OnlineCountResponse) | 获取在线设备数 |
| OnlineSessionList | [.OnlineListRequest](#OnlineListRequest) | [.OnlineSessionListResponse](#OnlineSessionListResponse) | 获取在线连接列表 |
| OnlineAccountList | [.OnlineListRequest](#OnlineListRequest) | [.OnlineAccountListResponse](#OnlineAccountListResponse) | 获取在线账号列表 |
| OnlineDeviceList | [.OnlineListRequest](#OnlineListRequest) | [.OnlineDeviceListResponse](#OnlineDeviceListResponse) | 获取在线设备列表 |
| CheckSession | [.CheckSessionRequest](#CheckSessionRequest) | [.CheckSessionResponse](#CheckSessionResponse) | 检查连接是否在线 |
| CheckAccount | [.CheckAccountRequest](#CheckAccountRequest) | [.CheckAccountResponse](#CheckAccountResponse) | 检查账号是否在线 |
| CheckDevice | [.CheckDeviceRequest](#CheckDeviceRequest) | [.CheckDeviceResponse](#CheckDeviceResponse) | 检查设备是否在线 |
| Refresh | [.RefreshStatRequest](#RefreshStatRequest) | [.RefreshStatResponse](#RefreshStatResponse) | 刷新统计 |

 



<a name="tools-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## tools.proto



<a name="-SendUpwardRequest"></a>

### SendUpwardRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| command | [string](#string) |  |  |
| body | [string](#string) |  |  |






<a name="-SendUpwardResponse"></a>

### SendUpwardResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| sent | [bool](#bool) |  |  |





 

 

 


<a name="-LinkUpward"></a>

### LinkUpward


| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| SendUpward | [.SendUpwardRequest](#SendUpwardRequest) | [.SendUpwardResponse](#SendUpwardResponse) | 上行消息 |

 



<a name="trace-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## trace.proto



<a name="-StartTraceRequest"></a>

### StartTraceRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tracer_type | [StartTraceRequest.TracerType](#StartTraceRequest-TracerType) |  |  |
| session_id | [string](#string) |  | 连接ID |
| device | [string](#string) |  | 设备标识 |
| account_id | [int64](#int64) |  | 账号ID |
| remote_addr | [string](#string) |  | 远程地址 |
| duration | [int64](#int64) |  | 持续时长 |






<a name="-StartTraceResponse"></a>

### StartTraceResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ok | [bool](#bool) |  | 状态 |
| trace_id | [string](#string) |  | Trace标识 |






<a name="-StopTraceRequest"></a>

### StopTraceRequest







<a name="-StopTraceResponse"></a>

### StopTraceResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ok | [bool](#bool) |  | 状态 |





 


<a name="-StartTraceRequest-TracerType"></a>

### StartTraceRequest.TracerType


| Name | Number | Description |
| ---- | ------ | ----------- |
| TRACER_ACCOUNT | 0 |  |
| TRACER_SESSION | 1 |  |
| TRACER_DEVICE | 2 |  |
| TRACER_REMOTE | 3 |  |
| TRACER_GLOBAL | 4 |  |


 

 


<a name="-LinkTrace"></a>

### LinkTrace


| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| Start | [.StartTraceRequest](#StartTraceRequest) | [.StartTraceResponse](#StartTraceResponse) | 设置Trace |
| Stop | [.StopTraceRequest](#StopTraceRequest) | [.StopTraceResponse](#StopTraceResponse) | 关闭Trace |

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

